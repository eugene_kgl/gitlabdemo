from django.contrib import admin
from .models import Book

class StoreAdminArea(admin.AdminSite):
    site_header='StoreArea'
store_site=StoreAdminArea(name='Thestore')

store_site.register(Book)
