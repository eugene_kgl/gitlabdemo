
from django.contrib import admin
from django.urls import path
from store.admin import store_site

urlpatterns = [
    path('admin/', store_site.urls),
]
